<!-- lISTA DE POSTS -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>	
	
	<!-- BEGIN .post class -->
	<div <?php post_class('blog-holder shadow radius-full'); ?> id="post-<?php the_ID(); ?>">

		<!--IMAGEM POST -->
		<?php if ( has_post_thumbnail() ) { ?>
			<a class="feature-img radius-top " href="<?php the_permalink(); ?>" rel="bookmark" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'swelllite' ), the_title_attribute( 'echo=0' ) ) ); ?>">
					
				<?php the_post_thumbnail( 'swell-featured-large img-post' );

				 ?>


			</a>
		<?php } ?>
		
		<!-- BEGIN .article -->
		<div class="article">
		
		
			<!--TITULO POST-->
			<h2 class="headline"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php esc_attr(the_title_attribute()); ?>"><?php the_title(); ?></a></h2>
			


			<?php if (get_theme_mod('display_author_blog', '1') == '1') { ?>
				<div class="post-author">
					<p><?php _e("Por", 'swelllite'); ?> <?php esc_url ( the_author_posts_link() ); ?></p>
				</div>
			<?php } ?>
			
			<span class="divider-small"></span>
		
			<?php the_content(__("Ler mais", 'swelllite')); ?>

			
		<!-- END .article -->
		</div>
	
	<!-- END .post class -->
	</div>

<?php endwhile; ?>

	<?php if ( $wp_query->max_num_pages > 1 ) { ?>
		<!-- BEGIN .pagination -->
		<div class="pagination">
			<?php echo swelllite_get_pagination_links(); ?>
		<!-- END .pagination -->
		</div>
	<?php } ?>

<?php else : ?>

	<div class="error-404">
		<h1 class="headline"><?php _e("No Posts Found", 'swelllite'); ?></h1>
		<p><?php _e("We're sorry, but no posts have been found. Create a post to be added to this section, and configure your theme options.", 'swelllite'); ?></p>
	</div>

<?php endif; ?>